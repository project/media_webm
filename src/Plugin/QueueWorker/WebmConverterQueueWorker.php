<?php

namespace Drupal\media_webm\Plugin\QueueWorker;

use Drupal\Core\File\FileSystem;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Queue\QueueInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use FFMpeg\Format\Video\WebM;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\file\Entity\File;
use FFMpeg\FFMpeg;

/**
 * Processes tasks for example module.
 *
 * @QueueWorker(
 *   id = "media_webm_converter",
 *   title = @Translation("WEBM Converter")
 * )
 */
class WebmConverterQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Constructs a new WebmConverterQueueWorker.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FileSystemInterface $file_system) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->fileSystem = $file_system;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('file_system')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $media_id = $data['media_id'];
    $source_uri = $data['source_uri'];
    // Load the media entity.
    $media = \Drupal::entityTypeManager()->getStorage('media')->load($media_id);
    if ($media && $media->bundle() === 'video') {
      $destination_uri = $this->convertToWebm($source_uri);

      // Create a file entity for the WEBM file.
      $webm_file = File::create([
        'uri' => $destination_uri,
        'uid' => $media->getOwnerId(),
        'status' => File::STATUS_PERMANENT,
      ]);
      $webm_file->save();

        // Update the media entity to reference the new WEBM file.
        // This assumes you have a field 'field_media_video_webm' for the WEBM file.
        $media->set('field_media_webm_uri', [
          'target_id' => $webm_file->id(),
        ]);
        //$media->isUpdating(TRUE);
        $media->save();
      }
  }

  /**
   * Converts an MP4 file to WEBM format and returns the destination URI.
   *
   * @param string $source_uri
   *   The URI of the source MP4 file.
   *
   * @return string
   *   The URI of the converted WEBM file.
   */
  protected function convertToWebm($source_uri) {
    // Determine the destination URI for the WEBM file.
    $destination_uri = $this->fileSystem->dirname($source_uri) . '/' . basename($source_uri, '.mp4') . '.webm';

    // Create an instance of the FFMpeg library.
    $ffmpeg = FFMpeg::create();
    $video = $ffmpeg->open($this->fileSystem->realpath($source_uri));
    $video->save(new WebM(), $this->fileSystem->realpath($destination_uri));

    // Use ffmpeg to convert the video.
//    $ffmpeg_command = "ffmpeg -i $source_uri -c:v libvpx -c:a libvorbis $destination_uri";
//    exec($ffmpeg_command);

    return $destination_uri;
  }

}
