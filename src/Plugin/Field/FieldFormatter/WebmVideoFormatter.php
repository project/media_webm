<?php

namespace Drupal\media_webm\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'WebmVideoFormatter'.
 *
 * @FieldFormatter(
 *   id = "webm_video_formatter",
 *   label = @Translation("WEBM Video Formatter"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class WebmVideoFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $delta => $item) {
      // Create render array for WEBM video.
      $elements[$delta] = [
        '#theme' => 'video_webm',
        '#url' => $item->entity->createFileUrl(),
        // Additional variables for template.
      ];
    }
    return $elements;
  }
}
