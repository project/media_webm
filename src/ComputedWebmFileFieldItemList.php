<?php

namespace Drupal\media_webm;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\file\Entity\File;

/**
 * Defines a computed field item list class for the WEBM file.
 */
class ComputedWebmFileFieldItemList extends FieldItemList {
  use ComputedItemListTrait;

  /**
   * Computes the value of the WEBM file field.
   */
  protected function computeValue() {
    $entity = $this->getEntity();
    // Check if the hidden field for the WEBM URI exists and has a value.
    if ($entity->hasField('field_media_webm_uri') && !$entity->get('field_media_webm_uri')->isEmpty()) {
      $webm_file_id = $entity->get('field_media_webm_uri')->target_id;
      $webm_file = File::load($webm_file_id);
      if ($webm_file) {
        // Create a file item and assign it to the list.
        $this->list[0] = $this->createItem(0, ['target_id' => $webm_file->id()]);
      }
    }
  }

  /**
   * Returns the referenced entities.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   The array of referenced entities.
   */
  public function referencedEntities() {
    $entities = [];
    foreach ($this->getValue() as $item) {
      if ($item['target_id'] && ($entity = File::load($item['target_id']))) {
        $entities[] = $entity;
      }
    }
    return $entities;
  }
}
