# Media WEBM Module Documentation

## Motivation

Serving videos efficiently on the web is a common challenge due to the variety of supported video formats across
different browsers and devices. While MP4 is a widely supported format, WEBM provides better compression and is
optimized for web delivery, making it a preferred choice for online video content. The Media WEBM module automates the
process of converting MP4 video files to WEBM within a Drupal site, ensuring compatibility and optimized delivery.

## Purpose

The Media WEBM module is designed to simplify the management and delivery of video content in WEBM format on Drupal
sites. It automates the conversion of MP4 files to WEBM upon upload, integrates with Drupal's Queue API for performance,
and provides a field formatter for displaying WEBM videos.

Features include:

- Automatic MP4 to WEBM video conversion.
- Transparent storage and management of WEBM files.
- Field formatter for displaying WEBM videos.
- Queue API integration for handling video conversions.

## How to Use

### Installation

1. Download and place the module in the `modules/custom` directory of your Drupal site.
2. Enable the module via the Drupal admin interface or with Drush:

```sh
drush en media_webm
```

### Configuration

1. Add a file field (e.g., `field_media_video_file`) to your media type for MP4 video uploads.
2. Ensure your video media type is configured to use the Media WEBM field formatter.

### Usage

1. Upload MP4 videos through the media type's file field. The module will enqueue a conversion job.
2. Manually process the conversion queue with the following Drush command:

```sh
drush queue:run media_webm_converter
```

Alternatively, set up a cron job to process the queue at regular intervals.

3. WEBM videos are automatically displayed using the module's field formatter.
